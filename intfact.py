#!/usr/bin/python3

import sys
from mpmath import *

PRECISION=512
def _complement(x):
    return (11 - int(x)) % 10

def get_probable_pairs(pair):
    nr = int(pair[0])
    dr = int(pair[1])
    c_nr = _complement(nr)
    c_dr = _complement(dr)
    probable_pairs = [[nr,dr],[dr,nr]]
    probable_pairs.append([nr, c_dr])
    probable_pairs.append([c_dr, nr])
    probable_pairs.append([c_nr, dr])
    probable_pairs.append([dr, c_nr])
    probable_pairs.append([c_nr, c_dr])
    probable_pairs.append([c_dr, c_nr])
    return probable_pairs

def getDigit(zz, sumz):
    zcnt = 0
    pos = 0
    while zcnt < sumz:
        zk = zz[pos]
        if zk != '0' and zk != '.':
            zcnt = zcnt + 1
        pos = pos + 1
    return zz[pos]

def getZeroDigit(pcount, ecount, psum, esum):
    pzero = str(zetazero(pcount).imag)
    ezero = str(zetazero(ecount).imag)
    pint = getDigit(pzero, psum)
    eint = getDigit(ezero, esum)
    return pint, eint


def factorize(lhalf, rhalf):
    fp = open("./pi.txt","r")
    fe = open("./e.txt","r")
    pcount = 0
    ecount = 0
    psum = 0
    esum = 0
    pos = 0
    nr = 0
    dr = 0
    factor_lte = ""
    factor_gte = ""
    prev_pos = -1
    for pair in list(zip(lhalf, rhalf)):
        probable_pairs = get_probable_pairs(pair)
        while True:
            pp = fp.read(1)
            ee = fe.read(1)
            if pp != '0' and pp != '.':
                pcount = pcount + 1
                psum = psum + int(pp)
            if ee != '0' and ee != '.':
                ecount = ecount + 1
                esum = esum + int(ee)
            if pp == '.' and ee == '.':
                pos = pos + 1
                continue
            if [int(pp), int(ee)] in probable_pairs:
                index = probable_pairs.index([int(pp), int(ee)])
    #            input([pp,ee, index])
                if index == 0: #[high/low, normal / normal]
                    pint, eint = getZeroDigit(pcount, ecount, psum, esum)
                    pint = int(pint)
                    eint = int(eint)
                    nr = eint
                    dr = pint
                elif index == 1: #[low/high, normal / normal]
                    pint, eint = getZeroDigit(pcount, ecount, psum, esum)
                    pint = int(pint)
                    eint = int(eint)
                    nr = pint
                    dr = eint
                elif index == 2: #[high/low, normal/complement]
                    pint, eint = getZeroDigit(pcount, ecount, psum, esum)
                    eint = _complement(eint)
                    pint = int(pint)
                    eint = int(eint)
                    nr = eint
                    dr = pint
                elif index == 3: #[low/high, complement/normal]
                    pint, eint = getZeroDigit(pcount, ecount, psum, esum)
                    pint = _complement(pint)
                    pint = int(pint)
                    eint = int(eint)
                    nr = pint
                    dr = eint
                elif index == 4: #[high/low, complement/normal]
                    pint, eint = getZeroDigit(pcount, ecount, psum, esum)
                    pint = _complement(pint)
                    pint = int(pint)
                    eint = int(eint)
                    nr = eint
                    dr = pint
                elif index == 5: #[low/high, normal/complement]
                    pint, eint = getZeroDigit(pcount, ecount, psum, esum)
                    eint = _complement(eint)
                    pint = int(pint)
                    eint = int(eint)
                    nr = pint
                    dr = eint
                elif index == 6: #[high/low, complement/complement]
                    pint, eint = getZeroDigit(pcount, ecount, psum, esum)
                    pint = _complement(pint)
                    eint = _complement(eint)
                    pint = int(pint)
                    eint = int(eint)
                    nr = eint
                    dr = pint
                elif index == 7: #[low/high, complement/complement]
                    pint, eint = getZeroDigit(pcount, ecount, psum, esum)
                    pint = _complement(pint)
                    eint = _complement(eint)
                    pint = int(pint)
                    eint = int(eint)
                    nr = pint
                    dr = eint
                input([pair, [int(pp), int(ee)], prev_pos, pos, [nr,dr]])
                prev_pos = pos
                pos = pos + 1
                break
            pos = pos + 1
    fp.close()
    fe.close()

if __name__ == "__main__":
    num = str(sys.argv[1])
    print("Number to be factored : " + str(num))
    l = len(num)
    mp.prec=PRECISION
    mp.dps=PRECISION
    mid = 0
    if l % 2 == 0:
        mid = int(l / 2)
        rhalf = num[mid:][::-1]
    else:
        mid = int((l + 1) / 2)
        rhalf = num[(mid-1):][::-1]
    lhalf = num[:mid]
    #print(lhalf)
    #print(rhalf)
    factor_lte, factor_gte = factorize(lhalf, rhalf)
    print(num + "\t=\t" + factor_lte + "\tX\t" + factor_gte)
